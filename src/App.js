import './App.css';
import {useState} from 'react'
// La logica que tenemos lo que gestion los datos el controlador
function App() {
 
    const [textA, SetTextA] = useState('');
    const [textB, SetTextB] = useState('');
    const [resultado, setResultado]= useState(0);
  
    const handleTextChangeA = (event) => {
       SetTextA(event.target.value)
       
  };

  const handleTextChangeB =(event) => {
    SetTextB(event.target.value)  
  };

  const sumar = () =>{
    setResultado(Number(textA) + Number(textB))

  }
//  apartado visual
  return (
    <div className="App">
      <input onChange={handleTextChangeA} type="text"/>
      <input onChange={handleTextChangeB}type="text"/>
      <button onClick={sumar}>
        <p> Calcular</p>
      </button>
       <p> El resultado es:{resultado} </p>
    </div>
  );
}

export default App;
